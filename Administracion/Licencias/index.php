<?php
session_cache_limiter('private');
$cache_limiter = session_cache_limiter();
session_cache_expire(3600);
session_start();
include("../../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
	header('Location:../');
}
elseif($_SESSION['MDE_CveSistema']!=0 && $_SESSION['MDE_CveSistema']!=1){
   echo "Error de credenciales de sesion, llamar al administrador del sistema! :(";
   echo "<br>Datos";
   echo "<br>Clave: ".$_SESSION['MDE_ClaveGeneral'];
   echo "<br>Sistema: ".$_SESSION['MDE_CveSistema'];
   echo "<br>Nivel: ".$_SESSION['MDE_NivelUsuario'];
   echo "<br><a href='$host/class/Cerrar.php'>".$_SESSION['MDE_NombreCompleto']."(Salir)</a> ";
   exit();
}
if($_SESSION['MDE_NivelUsuario']!=1 && $_SESSION['MDE_NivelUsuario']!=2){
	header('Location:../');
}
$Opc=2;

?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>MaletinDelEducador</title>
	<link href="<?php echo $host;?>img/LogoM.svg" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<!-- Bootstrap core CSS-->
	<link href="<?php echo $host;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?php echo $host;?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="<?php echo $host;?>css/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top" class="fixed-nav">
	<?php
   include($Ruta."/Administracion/MenuTop.php");
   ?>
	<div id="wrapper">
		<!-- Sidebar -->
		<?php
		include($Ruta."/Administracion/Menu.php");
		?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<div class="card">
					<div class="card-header">
						<div class="form-group">
								<h5>Filtros</h5>
							<div class="form-row">
								<div class="col-md-2 col-sm-6">
									<input type="text" id="txtbusqueda" onchange="Paginacion(1)" class="form-control text-primary" placeholder="Buscar">
								</div>
								<div class="col-md-2 col-sm-6">
									<select id="cmbEstatus" class="custom-select text-primary" onchange="Paginacion(1)">
										<option value="-1">Estatus</option>
										<option value="1">Asignada</option>
										<option value="0">Libre</option>
									</select>
								</div>
								<div class="col-md-1 col-sm-6 ml-auto">
									<button class="btn btn-block btn-success">
										<i class="fa fa-file-excel fa-1x mr-2"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div id="Tabla"></div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
			<?php include($Ruta."/Administracion/Pie.php");?>
		</div>
		<!-- /.content-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>
	<div id="ModaEditar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Datos de la licencia</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" class="form-control" id="txtidSerial">
						<label for="txtSerial">Serial</label>
						<input type="text" class="form-control" id="txtSerial" disabled>
					</div>
					<div class="form-group">
						<label for="txtEstatus">Estatus</label>
						<input type="text" class="form-control" id="txtEstatus" disabled>
					</div>
					<div class="form-group">
						<label for="txtActivacion">Fecha Activacion</label>
						<input type="text" class="form-control" id="txtActivacion" disabled>
					</div>
					<div class="form-group">
						<label for="txtPropietario">Propietario</label>
						<input type="text" class="form-control" id="txtPropietario">
					</div>
					<div class="form-group">
						<label for="txtObservaciones">Observaciones</label>
						<textarea class="form-control" id="txtObservaciones"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnGuardarEdit">Guardar</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Bootstrap core JavaScript-->
	<script src="<?php echo $host;?>vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo $host;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Core plugin JavaScript-->
	<script src="<?php echo $host;?>vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Page level plugin JavaScript-->
	<script src="<?php echo $host;?>vendor/chart.js/Chart.min.js"></script>
	<script src="<?php echo $host;?>vendor/datatables/jquery.dataTables.js"></script>
	<script src="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="<?php echo $host;?>js/sb-admin.min.js"></script>
	<script src="<?php echo $host;?>js/bootbox.min.js"></script>
	<script>
		//window.onload = document.getElementById("divCarga").style.display="none";
		$(window).on('load', function(){
			setTimeout(function() {
				$(".loader-page").css({
					visibility: "hidden",
					opacity: "0"
				})
			}, 500);
		});
		function EditarLicencias(Cve){
			$.post("InfoLicencias.php", {
				Cve: Cve
			},function(data) {
				$('#txtidSerial').val(data.id);
				$('#txtSerial').val(data.serial);
				$('#txtEstatus').val(data.estatus);
				$('#txtActivacion').val(data.fecha_activacion);
				$('#txtPropietario').val(data.propietario);
				$('#txtObservaciones').val(data.observaciones);
				console.log(data);
				
				$("#ModaEditar").modal();
			}, "json");
			
		}
		function Paginacion(pag){
			$.ajax({
				type: 'POST',
				url: 'PagLicencias.php',
				data: {
					pag: pag,
					busqueda: $("#txtbusqueda").val(),
					Estatus: $("#cmbEstatus").val()
				},
				beforeSend: function() {
					$('#Tabla').html("<div class='mx-auto loader'></div>");
				},
				success: function(data) {
					$("#Tabla").html(data);
				},
				error: function() {
					$("#Tabla").html("<div class='alert alert-danger text-center'><h3>No se encontro la pagina</h3></div>");
				}
			});
		}
		$("#btnGuardarEdit").click(function() {
			$.ajax({
				type: "POST",
				url: "EditarLicencia.php",
				dataType: "json",
				data: {
					Cve: $('#txtidSerial').val(),
					Propietario: $('#txtPropietario').val(),
					Observaciones: $('#txtObservaciones').val(),
				},
				cache: false,
				success: function (data) {
					bootbox.alert({
						message: data[0].text,
						callback: function () {
							location.reload();
						}
					});
				}
			});
			return false;
		});
		Paginacion(1);
	</script>
</body>
</html>
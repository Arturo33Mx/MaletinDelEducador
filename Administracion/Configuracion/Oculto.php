<?php
include("../../Class/rutas.php");

$Opc=0;
?>
<!DOCTYPE html>
<html lang="en">

<head>

   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="">
   <meta name="author" content="">
   <title>MaletinDelEducador</title>
   <link href="<?php echo $host;?>img/LogoM.svg" rel="shortcut icon" type="image/vnd.microsoft.icon">
   <!-- Bootstrap core CSS-->
   <link href="<?php echo $host;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <!-- Custom fonts for this template-->
   <link href="<?php echo $host;?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
   <!-- Page level plugin CSS-->
   <link href="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
   <!-- Custom styles for this template-->
   <link href="<?php echo $host;?>css/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top" class="fixed-nav">
   <?php
   include($Ruta."/Administracion/MenuTop.php");
   ?>
   <div id="wrapper">
      <!-- Sidebar -->
      <?php
      include($Ruta."/Administracion/Menu.php");
      ?>
      <div id="content-wrapper">
         <div class="container-fluid">
				<div class="form-group">
					<div class="card col-sm-6">
					
							<div class="card-body">
								<div class="form-group">
									<textarea type="text" id="txtDescripcion" class="form-control" placeholder=""></textarea>
									<button class="btn btn-block btn-primary" onclick="ejecutar()">ejecutar</button>
								</div>
							</div>
							<div class="card-body">
								<div id="respuesta"></div>
							</div>
					
					</div>
				</div>
			</div>
         <!-- /.container-fluid -->
         <!-- Sticky Footer -->
         <?php include($Ruta."/Administracion/Pie.php");?>
      </div>
      <!-- /.content-wrapper -->

   </div>
   <!-- /#wrapper -->

   <!-- Scroll to Top Button-->
   <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
   </a>
   <!-- Bootstrap core JavaScript-->
   <script src="<?php echo $host;?>vendor/jquery/jquery.min.js"></script>
   <script src="<?php echo $host;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- Core plugin JavaScript-->
   <script src="<?php echo $host;?>vendor/jquery-easing/jquery.easing.min.js"></script>
   <!-- Page level plugin JavaScript-->
   <script src="<?php echo $host;?>vendor/chart.js/Chart.min.js"></script>
   <script src="<?php echo $host;?>vendor/datatables/jquery.dataTables.js"></script>
   <script src="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.js"></script>
   <!-- Custom scripts for all pages-->
   <script src="<?php echo $host;?>js/sb-admin.min.js"></script>
   <script src="<?php echo $host;?>js/bootbox.min.js"></script>
	<script>
		$(window).on('load', function () {
         setTimeout(function () {
            $(".loader-page").css({visibility:"hidden",opacity:"0"})
         }, 500);
      });
		function ejecutar(){
         $.ajax({
            type: 'POST',
            url: 'script.php',
            data: {
               Descripc: $("#txtDescripcion").val()
            },
            beforeSend: function() {
               $('#respuesta').html("<div class='mx-auto loader'></div>");
            },
            success: function(data) {
               $("#respuesta").html(data);
               setTimeout(function () {
                  $(".loader-page").css({visibility:"hidden",opacity:"0"})
               }, 500);
            },
            error: function() {
               $("#respuesta").html("<div class='alert alert-danger text-center'><h3>No se encontro la pagina</h3></div>");
            }
         });
			return false;
      }
	</script>
</body>
</html>
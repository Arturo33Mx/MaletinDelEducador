<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
session_cache_limiter('private');
$cache_limiter = session_cache_limiter();
session_cache_expire(3600);
session_start();
include("../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
	header('Location:../');
}
elseif($_SESSION['MDE_CveSistema']!=0 && $_SESSION['MDE_CveSistema']!=1){
   echo "Error de credenciales de sesion, llamar al administrador del sistema! :(";
   echo "<br>Datos";
   echo "<br>Clave: ".$_SESSION['MDE_ClaveGeneral'];
   echo "<br>Sistema: ".$_SESSION['MDE_CveSistema'];
   echo "<br>Nivel: ".$_SESSION['MDE_NivelUsuario'];
   echo "<br><a href='".$host."class/Cerrar.php'>".$_SESSION['MDE_NombreCompleto']."(Salir)</a> ";
   exit();
}
if($_SESSION['MDE_NivelUsuario']!=1 && $_SESSION['MDE_NivelUsuario']!=2){
	header('Location:../');
}

$Opc=1;
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>MaletinDelEducador</title>
	<link href="<?php echo $host;?>img/LogoM.svg" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<!-- Bootstrap core CSS-->
	<link href="<?php echo $host;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?php echo $host;?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="<?php echo $host;?>css/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top" class="fixed-nav">
	<?php
   include($Ruta."Administracion/MenuTop.php");
   ?>
	<div id="wrapper">
		<!-- Sidebar -->
		<?php
		include($Ruta."Administracion/Menu.php");
		?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- Breadcrumbs-->
				<div class="row">
					<div class="col-md-8">
						<div class="card">
							<div class="card-header">
								<h5><i class="fa fa-chart-pie mr-3"></i>Registro de licencias</h5>
							</div>
							<div class="card-body" id="DivGraficaLicencias">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<h5><i class="fa fa fa-list mr-3"></i>Ultimos Movimientos</h5>
							</div>
							<div class="card-body p-0" id="DivMovimientos">

							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->
			<!-- Sticky Footer -->
			<?php include($Ruta."Administracion/Pie.php");?>
		</div>
		<!-- /.content-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>
	<!-- Bootstrap core JavaScript-->
	<script src="<?php echo $host;?>vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo $host;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Core plugin JavaScript-->
	<script src="<?php echo $host;?>vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Page level plugin JavaScript-->
	<script src="<?php echo $host;?>vendor/chart.js/Chart.min.js"></script>
	<script src="<?php echo $host;?>vendor/datatables/jquery.dataTables.js"></script>
	<script src="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="<?php echo $host;?>js/sb-admin.min.js"></script>
	<script src="<?php echo $host;?>js/bootbox.min.js"></script>
	<!-- -->
	<script src="<?php echo $host;?>vendor/Highcharts-6.2.0/highcharts.js"></script>
	<script>
		//window.onload = document.getElementById("divCarga").style.display="none";
		$(window).on('load', function() {
			setTimeout(function() {
				$(".loader-page").css({
					visibility: "hidden",
					opacity: "0"
				})
			}, 500);
		});

		function CargaGraficasLicencias() {
			$.ajax({
				type: 'POST',
				url: 'GraficaLicencias.php',
				beforeSend: function() {
					$('#DivGraficaLicencias').html("<div class='mx-auto loader'></div>");
				},
				success: function(data) {
					$("#DivGraficaLicencias").html(data);
				},
				error: function() {
					$("#DivGraficaLicencias").html("Error");
				}
			});
			$.ajax({
				type: 'POST',
				url: 'ListaMovimientos.php',
				beforeSend: function() {
					$('#DivMovimientos').html("<div class='mx-auto loader'></div>");
				},
				success: function(data) {
					$("#DivMovimientos").html(data);
				},
				error: function() {
					$("#DivMovimientos").html("Error");
				}
			});
		}
		CargaGraficasLicencias();
	</script>
</body>

</html>
<ul class="list-group list-group-flush">
	
<?php
session_start();
include("../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
   echo "<li class='list-group-item text-danger'><i class='fa fa-times-circle mr-2'></ierror</li>";
	exit;
}
require_once($Ruta.'Class/mysqli.php');
$hoy = date("Y-m-d g:i a");
//echo $Filtros;
$consulta="SELECT CONCAT_WS('-',Serie1,Serie2,Serie3,Serie4)serial, propietario, fecha_activacion FROM licencias where Estatus=1 limit 0,8;";
if($resultado = $bd->query($consulta)){
   if($resultado->num_rows>0){
		while ($fila = $resultado->fetch_assoc()) {
	?>
	<li class="list-group-item">
		<h6 class="text-primary">										
			<i class="fa fa-check-circle mr-3"></i>
			Se asigno la licencia <?php echo $fila['serial']?>
		</h6>
		<div class="row">
			<div class="small col">Al propietario <strong><?php echo $fila['propietario']?></strong></div>
			<div class="small col text-right"><i class="fa fa-calendar-check mr-1"></i><?php echo $fila['fecha_activacion']?></div>
		</div>
	</li>
	<?php
		}
   }
	else{
		echo "<li class='list-group-item text-warning'><i class='fa fa-minus-circle mr-2'></iNo Hay Registros</li>";
	}
}
else{
   echo "<li class='list-group-item bg-danger'><i class='fa fa-times-circle mr-2'></i>error al consultar los datos</li>";
}
?>
</ul>
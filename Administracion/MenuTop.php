<nav class="navbar navbar-expand navbar-dark bg-dark fixed-top">
   <a class="navbar-brand mr-1" href="index.html">MaletinDelEducador</a>
	<button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
</button>
   <!-- Navbar -->
   <ul class="navbar-nav ml-auto ">
      <li class="nav-item dropdown no-arrow">
         <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
         </a>
         <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item"><?php echo $_SESSION['MDE_NombreCompleto'];?></a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" onclick="SalirPerfil();">Salir</a>
         </div>
      </li>
   </ul>
</nav>
<script>
   function SalirPerfil(){
      bootbox.confirm({
         message: "<h4>Seguro que desea salir?</h4>",
         buttons: {
            cancel: {
               label: '<i class=""></i> Aun no!',
               className: 'btn-primary'
            },
            confirm: {
               label: '<i class="fa fa-times"></i> Si!',
               className: 'btn-danger'
            }
         },
         callback: function (result) {
            if(result){
                 location.href = "<?php echo $host; ?>/Class/Cerrar.php";
            }
         }
      });
   }
</script>
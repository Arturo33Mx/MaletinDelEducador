$(document).ready(function () {
	$("#frmLogin").submit(function (event) {
		var nombre = $('#txtUsuario').val();
		var password = $('#txtPass').val();
		var ip1 = $('#IP').val();
		$.ajax({
			type: "POST",
			url: "Class/login.php",
			dataType: "json",
			data: {
				nombre: nombre,
				password: password,
			},
			cache: false,
			success: function (data) {
				console.log(data);

				if (data[0].Cve == 1) {
					bootbox.alert({
						message: data[0].text,
						callback: function () {
							document.location.href = "Direccionamiento.php";
						}
					})
				} else if (data[0].Cve == 405) {
					bootbox.alert(data[0].text);
				} else if (data[0].Cve == 406) {
					bootbox.alert(data[0].text);
				} else if (data[0].Cve == 407) {
					bootbox.alert("Error de base de datos:<br>" + data[0].text);
				} else {
					bootbox.alert(data[0].text);
				}
			}
		});
		event.preventDefault();
		return false;
	});
});

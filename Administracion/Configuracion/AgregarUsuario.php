<?php
session_start();
include("../../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
   echo "aqui";
	exit;
}
require_once($Ruta.'Class/mysqli.php');
$text="";
$Cve=0;
if(empty($_POST)){
	echo 'error_post';
	exit;
}
else{
	$Usuario=trim($_POST['Usuario']);
	$Nombre=utf8_decode(trim($_POST['Nombre']));
	$Paterno=utf8_decode(trim($_POST['Paterno']));
	$Materno=utf8_decode(trim($_POST['Materno']));
	$Password=trim($_POST['Password']);
	$Nivel=$_POST['Nivel'];
    $Password=password_hash($Password, PASSWORD_BCRYPT);
}
$sql="INSERT INTO usuarios(Usuario, Pass, CveNivel, Estatus, Nombre, ApePaterno, ApeMaterno)
VALUES(?, ?, ?, 1, ?, ?, ?);";
/* Sentencia preparada, etapa 1: preparación */
if(!($sentencia = $bd->prepare($sql))){
	$text.= "Falló la preparación: (" . $bd->errno . ") " . $bd->error;
}
else{   
	/* Sentencia preparada, etapa 2: vinculación y ejecución */
	if (!$sentencia->bind_param("ssisss", $Usuario, $Password, $Nivel, $Nombre, $Paterno, $Materno)) {
		$text.= "Falló la vinculación de parámetros: (" . $sentencia->errno . ") " . $sentencia->error;
	}
	else{
		/* Sentencia preparada: ejecución */
		if (!$sentencia->execute()) {
			$text.= "Falló la ejecución: (" . $sentencia->errno . ") " . $sentencia->error;
		}
		else{
			$Cve=1;
			$text.= "Usuario creado correctamente!";
		}
	}
}
$Row ['Clave'] = $Cve;
$Row ['text'] = $text;
$Json[] = $Row;
echo json_encode($Json);
<?php
session_start();
include("../../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
   echo "aqui";
	exit;
}
require_once($Ruta.'Class/mysqli.php');
$hoy = date("Y-m-d g:i a");
$RegistrosAMostrar=10;

if(isset($_POST['pag'])){
	$RegistrosAEmpezar=( $_POST['pag']-1) * $RegistrosAMostrar;
	$PagAct=$_POST['pag'];
}
else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
$Total=$RegistrosAEmpezar+$RegistrosAMostrar;
$Filtros="";
if(isset($_POST['busqueda'])){
   if(strlen($_POST['busqueda'])>0){
      $Filtros.=" and (serial like '%".$_POST['busqueda']."%'";
      $Filtros.=" or propietario like '%".$_POST['busqueda']."%'";
      $Filtros.=" or observaciones like '%".$_POST['busqueda']."%')";
   }
}
if(isset($_POST['Estatus'])){
   if($_POST['Estatus']>=0){
      $Filtros.=" and estatus=".$_POST['Estatus'];
   }
}
?>
<div class="row">
	<div class="col-sm-9">
		<ul class="pagination pagination-sm">
			<?php
			$NroRegistros=0;
			$consulta = "SELECT id FROM licencias where 1 $Filtros";
			if($sentencia = $bd->prepare($consulta)) {
				$sentencia->execute();
				$sentencia->store_result();
				$NroRegistros = $sentencia->num_rows;
				$sentencia->close();
			}
			if($NroRegistros==0){
				echo "<div>No Hay Registros<div>";
			}
			else{
				$PagAnt = $PagAct-1;
				$PagSig = $PagAct+1;
				$PagUlt = $NroRegistros/$RegistrosAMostrar;
				$Res=$NroRegistros%$RegistrosAMostrar;
				if($Res>0){
					$PagUlt=floor($PagUlt)+1;
				}
				$StrMenu="";
				/*INICIO*/
				$SobAnt=0;
				if($PagAct<=4){
					$SobAnt=4-$PagAct;	
				}
				$SobSig=0;
				if(($PagUlt-$PagAct)<=3){
					$SobSig=3-($PagUlt-$PagAct);
				}
				if($PagAct > 1){
					$StrMenu.= "<li class='paginate_button page-item previous'>";
					$StrMenu.= "<a href='#' onclick='Paginacion(".($PagAct-1).")' aria-controls='dataTable' tabindex='0' class='page-link'>Anterior</a>";
					$StrMenu.= "</li>";
				}
				else{
					$StrMenu.= "<li class='paginate_button page-item previous disabled'>";
					$StrMenu.= "<a href='#' aria-controls='dataTable' tabindex='0' class='page-link'>Anterior</a>";
					$StrMenu.= "</li>";
				}
				/*ANTES DE LA ACTUAL */
				if($PagAct<=4){
					$Pos=1;
					while($Pos<$PagAct){
						$StrMenu.= "<li class='paginate_button page-item'>";
						$StrMenu.= "<a href='#' onclick='Paginacion($Pos)' aria-controls='dataTable' tabindex='0' class='page-link'>$Pos</a>";
						$StrMenu.= "</li>";
						$Pos++;
					}
				}
				else{
					$Pos=$PagAct-1;
					if($Pos-$SobSig<3){
						$Pos=3;
					}
					else{
						$Pos=$Pos-$SobSig;
					}
					$StrMenu.= "<li class='paginate_button page-item'>";
					$StrMenu.= "<a href='#' onclick='Paginacion(1)' aria-controls='dataTable' tabindex='0' class='page-link'>".(1)."</a>";
					$StrMenu.= "</li>";
					if($Pos>3){
						$StrMenu.= "<li class='paginate_button page-item next disabled' id='dataTable_next'>";
						$StrMenu.= "<a href='' aria-controls='dataTable' data-dt-idx='7' tabindex='0' class='page-link'>...</a>";
						$StrMenu.= "</li>";
					}
					else{
						$StrMenu.= "<li class='paginate_button page-item next' id='dataTable_next'>";
						$StrMenu.= "<a href='#' onclick='Paginacion(2)' aria-controls='dataTable' data-dt-idx='7' tabindex='0' class='page-link'>2</a>";
						$StrMenu.= "</li>";
					}
					while($Pos<$PagAct){
						$StrMenu.= "<li class='paginate_button page-item'>";
						$StrMenu.= "<a href='#' onclick='Paginacion($Pos)' aria-controls='dataTable' tabindex='0' class='page-link'>$Pos</a>";
						$StrMenu.= "</li>";
						$Pos++;
					}
				}
				/* ACTUAL */
				$StrMenu.= "<li class='paginate_button page-item active'>";
				$StrMenu.= "<a aria-controls='dataTable' tabindex='0' class='page-link'>".($PagAct)."</a>";
				$StrMenu.= "</li>";
				/*DESPUES DE LA ACTUAL */
				if(($PagUlt-$PagAct)<=3){
					$Pos=$PagAct;
					while($Pos<$PagUlt){
						$Pos++;
						$StrMenu.= "<li class='paginate_button page-item'>";
						$StrMenu.= "<a href='#' onclick='Paginacion($Pos)' aria-controls='dataTable' tabindex='0' class='page-link'>$Pos</a>";
						$StrMenu.= "</li>";
					}
				}
				else{
					if(($PagAct+$SobAnt+1)<$PagUlt){
						$PosFinal=$PagAct+$SobAnt+1;	
					}
					else{
						$PosFinal=$PagUlt-1;
					}
					$Pos=$PagAct+1;
					while($Pos<=$PosFinal){
						$StrMenu.= "<li class='paginate_button page-item'>";
						$StrMenu.= "<a href='#' onclick='Paginacion($Pos)' aria-controls='dataTable' tabindex='0' class='page-link'>$Pos</a>";
						$StrMenu.= "</li>";
						$Pos++;
					}
					if($PosFinal<$PagUlt-1){
						$StrMenu.= "<li class='paginate_button page-item next disabled' id='dataTable_next'>";
						$StrMenu.= "<a href='' aria-controls='dataTable' data-dt-idx='7' tabindex='0' class='page-link'>...</a>";
						$StrMenu.= "</li>";
					}
					$StrMenu.= "<li class='paginate_button page-item'>";
					$StrMenu.= "<a href='#' onclick='Paginacion($PagUlt)' aria-controls='dataTable' tabindex='0' class='page-link'>".($PagUlt)."</a>";
					$StrMenu.= "</li>";
				}
				/*FIN*/
				if($PagAct < $PagUlt){
					$StrMenu.= "<li class='paginate_button page-item next'>";
					$StrMenu.= "<a href='#' onclick='Paginacion($PagSig)'  aria-controls='dataTable' data-dt-idx='7' tabindex='0' class='page-link'>Siguiente</a>";
					$StrMenu.= "</li>";
				}
				else{
					$StrMenu.= "<li class='paginate_button page-item next disabled' id='dataTable_next'>";
					$StrMenu.= "<a href='' aria-controls='dataTable' data-dt-idx='7' tabindex='0' class='page-link'>Siguiente</a>";
					$StrMenu.= "</li>";
				}
				echo $StrMenu;
			}
			?>
		</ul>
	</div>
	<div class="col-sm-3 text-right">
		<?php
		if($Total>$NroRegistros){
			$Total=$NroRegistros;
		}
		echo "<strong>$RegistrosAEmpezar</strong> al <strong>$Total</strong> de <strong>$NroRegistros</strong> registros";
		?>
	</div>
</div>
<div class="table-responsive">
	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th>Clave</th>
				<th>Serial</th>
				<th>Estatus</th>
				<th>Fecha Activacion</th>
				<th>Propietario</th>
				<th>Observaciones</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$consulta = "SELECT id, CONCAT_WS('-',Serie1,Serie2,Serie3,Serie4)serial, CASE estatus 
			WHEN 0 THEN 'Libre' 
			WHEN 1 THEN 'Asignada' 
			END estatusDes, estatus, propietario, fecha_activacion, observaciones FROM licencias where 1 $Filtros LIMIT $RegistrosAEmpezar, $RegistrosAMostrar;";
			if ($resultado = $bd->query($consulta)) {
				/* obtener un array asociativo */
				while ($fila = $resultado->fetch_assoc()) {
					if($fila['estatus']==0){
						$Class="text-azul-asignado";
					}
					else{
						$Class="text-warning";
					}
			?>
			<tr>
				<td>
					<?php echo ($fila['id']); ?>
				</td>
				<td>
					<?php echo ($fila['serial']); ?>
				</td>
				<td class="<?php echo $Class; ?>">
					<?php echo ($fila['estatusDes']); ?>
				</td>
				<td>
					<?php echo ($fila['fecha_activacion']); ?>
				</td>
				<td>
					<?php echo utf8_encode($fila['propietario']); ?>
				</td>
				<td>
					<?php echo utf8_encode($fila['observaciones']); ?>
				</td>
				<td>
					<i class="fa fa-edit text-primary fa-2x" onclick="EditarLicencias('<?php echo $fila['id'];?>')"></i>
				</td>
			</tr>
			<?php
				}
				/* liberar el conjunto de resultados */
				$resultado->free();
			}
			else{
			}
			//echo $consulta;
			/* cerrar la conexión */
			$bd->close();
			
			?>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-sm-9">
		<ul class="pagination pagination-sm">
			<?php 
			if($NroRegistros>0){
				echo $StrMenu; 
			}
			?>
		</ul>
	</div>
	<div class="col-sm-3 text-right">
		<?php
		if($Total>$NroRegistros){
			$Total=$NroRegistros;
		}
		echo "<strong>$RegistrosAEmpezar</strong> al <strong>$Total</strong> de <strong>$NroRegistros</strong> registros";
		?>
	</div>
</div>
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>
<?php
session_cache_limiter('private');
$cache_limiter = session_cache_limiter();
session_cache_expire(3600);
session_start();
include("../../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
	header('Location:../');
}
elseif($_SESSION['MDE_CveSistema']!=0 && $_SESSION['MDE_CveSistema']!=1){
   echo "Error de credenciales de sesion, llamar al administrador del sistema! :(";
   echo "<br>Datos";
   echo "<br>Clave: ".$_SESSION['MDE_ClaveGeneral'];
   echo "<br>Sistema: ".$_SESSION['MDE_CveSistema'];
   echo "<br>Nivel: ".$_SESSION['MDE_NivelUsuario'];
   echo "<br><a href='$host/class/Cerrar.php'>".$_SESSION['MDE_NombreCompleto']."(Salir)</a> ";
   exit();
}
if($_SESSION['MDE_NivelUsuario']!=1){
	header('Location:../');
}
$Opc=3;
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>MaletinDelEducador</title>
	<link href="<?php echo $host;?>img/LogoM.svg" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<!-- Bootstrap core CSS-->
	<link href="<?php echo $host;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?php echo $host;?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="<?php echo $host;?>css/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top" class="fixed-nav">
	<?php
   include($Ruta."/Administracion/MenuTop.php");
   ?>
	<div id="wrapper">
		<!-- Sidebar -->
		<?php
      include($Ruta."/Administracion/Menu.php");
      ?>
		<div id="content-wrapper">
			<div class="container-fluid">
				<div class="breadcrumb">
					<h4>Usuarios</h4>
				</div>
				<div class="form-group">
					<div class="card">
						<form id="miForm">
							<div class="card-header">
								<div class="form-group">
									<div class="form-row">
										<div class="col-md-2 col-sm-6">
											<input type="text" id="txtDescripcion" onchange="Paginacion()" class="form-control" placeholder="Buscar">
										</div>
										<div class="col-md-1 col-sm-6 ml-auto">
											<button class="btn btn-block btn-primary" id="btnNuevo" title="Agregar Usuario">
												<i class="fa fa-plus"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div id="Tabla"></div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php include($Ruta."/Administracion/Pie.php");?>
			<div id="ModalNuevoUsuario" class="modal fade" tabindex="-1" role="dialog" data-spy="scroll">
				<div class="modal-dialog " role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Datos del nuevo usuario</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div> <!-- header-->
						<div class="modal-body">
							<div class="form-group row">
								<div class="col-md-4">
									<label for="txtNuevoUserNombre">Nombre</label>
									<input type="text" class="form-control" id="txtNuevoUserNombre">
								</div>
								<div class="col-md-4">
									<label for="txtNuevoUserPaterno">Ape Paterno</label>
									<input type="text" class="form-control" id="txtNuevoUserPaterno">
								</div>
								<div class="col-md-4">
									<label for="txtNuevoUserMaterno">Ape Materno</label>
									<input type="text" class="form-control" id="txtNuevoUserMaterno">
								</div>
							</div>
						   <div class="form-group row">
								<div class="col-md-8">
									<label for="txtNuevoUserUsuario">Usuario</label>
									<input type="text" class="form-control" id="txtNuevoUserUsuario" onkeyup="ValidaUsuario()">
								</div>
								<div class="col-md-4 my-auto">
									<i id="Disponible" class="fa fa-check-circle text-primary" style="display:none"> Disponible</i>
									<i id="NoDisponible" class="fa fa-times-circle text-danger" style="display:none"> No disponible</i>
									<input type="hidden" class="form-control" id="txtDisponible" value="0">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6">
									<label for="txtNuevoUserPassword1">Contraseña</label>
									<input type="password" class="form-control" id="txtNuevoUserPassword1" placeholder="*****" onchange="ValidaPass()">
								</div>
								<div class="col-md-6">
									<label for="txtNuevoUserPassword2">Repite la Contraseña</label>
									<input type="password" class="form-control" id="txtNuevoUserPassword2" placeholder="*****" onchange="ValidaPass()">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-12">
									<label for="cmbNuevoUserNivel">Nivel</label>
									<select class="form-control custom-select" id="cmbNuevoUserNivel">
										<option value="0">Nivel</option>
										<option value="1">root</option>
										<option value="2">Administrador</option>
									</select>
								</div>
							</div>
						</div><!-- body-->
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnGuardarNuevoUser">Guardar</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
						</div>
						<!--footer-->
					</div>
				</div>
			</div><!-- nuevo usuario-->
		</div>
		<!-- /.content-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>
	<!-- Bootstrap core JavaScript-->
	<script src="<?php echo $host;?>vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo $host;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Core plugin JavaScript-->
	<script src="<?php echo $host;?>vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Page level plugin JavaScript-->
	<script src="<?php echo $host;?>vendor/chart.js/Chart.min.js"></script>
	<script src="<?php echo $host;?>vendor/datatables/jquery.dataTables.js"></script>
	<script src="<?php echo $host;?>vendor/datatables/dataTables.bootstrap4.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="<?php echo $host;?>js/sb-admin.min.js"></script>
	<script src="<?php echo $host;?>js/bootbox.min.js"></script>
	<script>
		$(window).on('load', function() {
			setTimeout(function() {
				$(".loader-page").css({
					visibility: "hidden",
					opacity: "0"
				})
			}, 500);
		});
		function ValidaPass(){
			if($('#txtNuevoUserPassword1').val()==$('#txtNuevoUserPassword2').val()){
				$('#txtNuevoUserPassword1').removeClass('is-invalid');
				$('#txtNuevoUserPassword2').removeClass('is-invalid');
			}
			else{
				$('#txtNuevoUserPassword1').addClass('is-invalid');
				$('#txtNuevoUserPassword2').addClass('is-invalid');
			}
		}
		function ValidaUsuario(){
			if($('#txtNuevoUserUsuario').val().length>3){
			   $.ajax({
					type: "POST",
					url: "ValidaUsuario.php",
					dataType: "json",
					data: {
						Usuario: $('#txtNuevoUserUsuario').val(),
					},
					cache: false,
					success: function (data) {
						console.log(data);
						$('#txtDisponible').val(data[0].Clave);
						if(data[0].Clave==1){
							$('#NoDisponible').hide();
							$('#Disponible').show();
						}
						else{
							$('#NoDisponible').show();
							$('#Disponible').hide();
						}						
					}
				});
			}
			else{
				$('#NoDisponible').show();
				$('#Disponible').hide();
			}
			return false;
		}
		$("#btnGuardarNuevoUser").click(function(){
			$('#txtNuevoUserNombre').removeClass('is-invalid');
			$('#txtNuevoUserPaterno').removeClass('is-invalid');
			$('#txtNuevoUserMaterno').removeClass('is-invalid');
			$('#txtNuevoUserUsuario').removeClass('is-invalid');
			$('#cmbNuevoUserNivel').removeClass('is-invalid');
			err = false;
			var Mensaje="";
			if($('#txtNuevoUserPassword1').val().length<8){
				$('#txtNuevoUserPassword1').addClass('is-invalid');
				$('#txtNuevoUserPassword2').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* La contraseña tiene que ser mayor a 8 caracteres";
			}
			if($('#txtNuevoUserPassword1').val()!=$('#txtNuevoUserPassword2').val()){
				$('#txtNuevoUserPassword1').addClass('is-invalid');
				$('#txtNuevoUserPassword2').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* La contraseña no coincide";
			}
			if($('#txtNuevoUserNombre').val().length <= 2) {
				$('#txtNuevoUserNombre').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* Falta el nombre";
			}
			if($('#txtNuevoUserPaterno').val().length <= 2) {
				$('#txtNuevoUserPaterno').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* Falta el apellido paterno";
			}
			if($('#txtNuevoUserMaterno').val().length <= 2) {
				$('#txtNuevoUserMaterno').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* Falta el apellido materno";
			}
			if($('#txtNuevoUserUsuario').val().length <= 2) {
				$('#txtNuevoUserUsuario').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* Falta el usuario";
			}
			if($('#cmbNuevoUserNivel').val() == 0){
				$('#cmbNuevoUserNivel').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* Falta el nivel";
			}
			if($('#txtDisponible').val() == 2){
				$('#txtDisponible').addClass('is-invalid');
				err = true;
				Mensaje += "<br>* El usuario no esta disponible";
			}
			if(err){
				bootbox.alert({
					size: "small",
					message: '<h3>Datos Incompletos</h3>' + Mensaje,
				});
				return false;
			}
			else {
				$.ajax({
					type: "POST",
					url: "AgregarUsuario.php",
					dataType: "json",
					data: {
						Nombre: $('#txtNuevoUserNombre').val(),
						Paterno: $('#txtNuevoUserPaterno').val(),
						Materno: $('#txtNuevoUserMaterno').val(),
						Usuario: $('#txtNuevoUserUsuario').val(),
						Password: $('#txtNuevoUserPassword1').val(),
						Nivel: $('#cmbNuevoUserNivel').val(),
					},
					cache: false,
					success: function (data) {
						bootbox.alert({
							message: data[0].text,
							callback: function () {
								location.reload();
							}
						});
					}
				});
			}
			return false;
		});
		function Paginacion(pag) {
			$.ajax({
				type: 'POST',
				url: 'PagUsuarios.php',
				data: {
					pag: pag,
					Descripc: $("#txtDescripcion").val()
				},
				beforeSend: function() {
					$('#Tabla').html("<div class='mx-auto loader'></div>");
				},
				success: function(data) {
					$("#Tabla").html(data);
					setTimeout(function() {
						$(".loader-page").css({
							visibility: "hidden",
							opacity: "0"
						})
					}, 500);
				},
				error: function() {
					$("#Tabla").html("<div class='alert alert-danger text-center'><h3>No se encontro la pagina</h3></div>");
				}
			});
		}
		$("#btnNuevo").click(function() {
			$("#ModalNuevoUsuario").modal();
			return false;
		});
		Paginacion(1);
	</script>
</body>

</html>
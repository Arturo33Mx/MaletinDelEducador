<?php 
session_start();
include("../../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
   echo "aqui";
	exit;
}
if(empty($_POST)){
	echo 'error_post';
	exit;
}
else{
	$Cve=$_POST['Cve'];
}
require_once($Ruta.'Class/mysqli.php');
$data= array();
$consulta="SELECT id, fecha_activacion, CONCAT_WS('-',Serie1,Serie2,Serie3,Serie4)serial, propietario, observaciones,
CASE estatus
WHEN 0 THEN 'Libre'
WHEN 1 THEN 'Asignada'
END estatus
FROM licencias where id=$Cve";
if($resultado = $bd->query($consulta)){
	if($resultado->num_rows>0){
		if ($fila = $resultado->fetch_assoc()) {
			$data = ($fila);
			$data['propietario']=utf8_encode($data['propietario']);
			$data['observaciones']=utf8_encode($data['observaciones']);
		}
	}
}
else{
	echo "Error";
}
echo json_encode($data);
?>

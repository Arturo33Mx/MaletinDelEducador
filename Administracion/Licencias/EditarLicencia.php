<?php
session_start();
include("../../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
   echo "aqui";
	exit;
}
require_once($Ruta.'Class/mysqli.php');
$text="";

if(empty($_POST)){
	echo 'error_post';
	exit;
}
else{
	$Cve=$_POST['Cve'];
	$Propietario=utf8_decode($_POST['Propietario']);
	$Observaciones=utf8_decode($_POST['Observaciones']);
}
$sql="UPDATE licencias SET propietario = ?, observaciones = ? WHERE id = ?;";
/* Sentencia preparada, etapa 1: preparación */
if(!($sentencia = $bd->prepare($sql))){
	$text.= "Falló la preparación: (" . $bd->errno . ") " . $bd->error;
}
else{   
	/* Sentencia preparada, etapa 2: vinculación y ejecución */
	if (!$sentencia->bind_param("ssi", $Propietario,$Observaciones,$Cve)) {
		$text.= "Falló la vinculación de parámetros: (" . $sentencia->errno . ") " . $sentencia->error;
	}
	else{
		/* Sentencia preparada: ejecución */
		if (!$sentencia->execute()) {
			$text.= "Falló la ejecución: (" . $sentencia->errno . ") " . $sentencia->error;
		}
		else{
			$text.= "Licencia modificada correctamente!";
		}
	}
}
$Row ['Clave'] = $Cve;
$Row ['text'] = $text;
$Json[] = $Row;
echo json_encode($Json);
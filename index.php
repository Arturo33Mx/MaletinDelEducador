<?php
session_start();
include("Class/rutas.php");
if(isset($_SESSION['MDE_ClaveGeneral'])){
   header('Location:Direccionamiento.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta name="description" content="">
   <meta name="author" content="">

   <title>Admin - Educador</title>
   <link href="<?php echo $host;?>img/LogoM.svg" rel="shortcut icon" type="image/vnd.microsoft.icon">
   <!-- Bootstrap core CSS-->
   <link href="<?php echo $host;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <!-- Custom fonts for this template-->
   <link href="<?php echo $host;?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
   <!-- Custom styles for this template-->
   <link href="<?php echo $host;?>css/sb-admin.css" rel="stylesheet">
</head>
<body class="bg-dark">
   <div class="container">
      <div class="card card-login mx-auto mt-5">
         <div class="card-body">
            <form id="frmLogin">
               <div class="form-group text-center">
                  
                  <img class="" style="width:70%; margin-top:-50px;" src="<?php echo $host;?>img/LogoM.svg">
                  <h4 class="">Maletín del Educador <strong>Administrador</strong></h4>
               </div>
               <div class="form-group">
                  <label class="sr-only" for="txtUsuario">Usuario</label>
                  <div class="input-group">
                     <div class="input-group-prepend">
                        <i class="input-group-text fa fa-user pt-3"></i>
                     </div>
                     <input type="text" class="form-control form-control-lg" id="txtUsuario" placeholder="Usuario" required>
                  </div>
               </div>
               <div class="form-group">
                  <label class="sr-only" for="txtPass">Contraseña</label>
                  <div class="input-group">
                     <div class="input-group-prepend">
                        <i class="input-group-text fa fa-lock pt-3"></i>
                     </div>
                     <input type="password" id="txtPass" class="form-control form-control-lg" placeholder="Contraseña" required="required">
                  </div>
               </div>
               <button class="btn btn-primary btn-block">Inicio</button>
            </form>
         </div>
      </div>
   </div>
   <!-- Bootstrap core JavaScript-->
   <script src="<?php echo $host;?>vendor/jquery/jquery.min.js"></script>
   <script src="<?php echo $host;?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- Core plugin JavaScript-->
   <script src="<?php echo $host;?>vendor/jquery-easing/jquery.easing.min.js"></script>
   <script src="<?php echo $host;?>Login.js"></script>
	<script src="<?php echo $host;?>js/bootbox.min.js"></script>
</body>
</html>
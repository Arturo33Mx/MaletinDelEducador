<?php
session_start();
include("../Class/rutas.php");
if(!isset($_SESSION['MDE_ClaveGeneral'])){
   echo "aqui";
	exit;
}
require_once($Ruta.'Class/mysqli.php');
$hoy = date("Y-m-d g:i a");
//echo $Filtros;
$consulta="SELECT COUNT(id) Total,
CASE estatus 
WHEN 0 THEN 'Libres' 
WHEN 1 THEN 'Asignadas' 
END Descripcion
FROM licencias GROUP BY estatus";
if($resultado = $bd->query($consulta)){
   if($resultado->num_rows>0){
      $datos="[";
      while ($fila = $resultado->fetch_assoc()) {
         $datos = $datos."['".$fila['Descripcion']."', ".$fila['Total']."],";
      }
      $datos=$datos."]";
   }
}
else{
   echo "<div>No Hay Registros<div>";
   $datos="[]";
}
?>
<div id="CharLicencias" class="col-md-12" style="height: 450px;"></div>
<script>
	var options = {
		chart: {
			renderTo: 'CharLicencias',
			type: 'pie'
		},
		colors: ['#c7ef00', '#067bc2'],
		title: {
			text: 'Datos cargados al: <?php echo $hoy;?>',
			style: {
				fontSize: '18px'
			}
		},
		tooltip: {
			pointFormat: '',
			style: {
				fontSize: '13px'
			}
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,
				dataLabels: {
					enabled: true,
					format: '{point.name} / <b>{point.y}</b>',
					style: {
						fontSize: '13px'
					}
				},
				showInLegend: true
			}
		},
		series: [{
			data: <?php echo $datos; ?>
			/*
			data:[['Firefox', 45.0],['IE', 26.8],['Safari', 8.5],['Opera', 6.2],['Others', 0.7]]
			*/
		}]
	};
	var chart = new Highcharts.Chart(options);
	//setTimeout(requestData, 50000);

</script>

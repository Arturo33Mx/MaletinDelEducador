<?php
$url = $host."Administracion/";
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>
<ul class="sidebar navbar-nav fixed-top">
	<li class="nav-item <?php if($Opc==1){ echo " active";} ?>">
		<a class="nav-link" href="<?php echo $url; ?>index.php">
			<i class="fas fa-fw fa-chart-area"></i>
			<span>Inicio</span>
		</a>
	</li>
	<li class="nav-item <?php if($Opc==2){ echo " active";} ?>">
		<a class="nav-link" href="<?php echo $url; ?>Licencias/">
			<i class="fas fa-fw fa-key"></i>
			<span>Licencias</span>
		</a>
	</li>
	<?php
	if($_SESSION['MDE_NivelUsuario']==1){
	?>
	<li class="nav-item <?php if($Opc==3){ echo " active";} ?>">
		<a class="nav-link" href="<?php echo $url; ?>Configuracion/">
			<i class="fas fa-fw fa-users"></i>
			<span>Usuarios</span>
		</a>
	</li>
	<li class="nav-item <?php if($Opc==4){ echo " active";} ?>">
		<a class="nav-link" href="<?php echo $url; ?>AdminLicencias/">
			<i class="fas fa-fw fa-cogs"></i>
			<span>Admin licencias</span>
		</a>
	</li>
	<?php
	}
	?>
</ul>
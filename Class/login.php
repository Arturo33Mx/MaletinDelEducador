<?php
clearstatcache();
session_start();
if(empty($_POST)){
	echo 'error_post';
	exit();
}
else{
	$txtNombre=$_POST['nombre'];
	$txtPass=$_POST['password'];
}
require_once('mysqli.php');
$cve=0;
$text="";
$SqlTex="SELECT Clave, Usuario, Pass, CveSistema, CveNivel, CONCAT_WS(' ',Nombre,ApePaterno,ApeMaterno)Nombre FROM usuarios
where Usuario=? and Estatus=1";

if(($sentencia = $bd->prepare($SqlTex))) {
   if($sentencia->bind_param("s",$txtNombre)) {
      $sentencia->execute();
      $sentencia->bind_result($Clave, $Usuario, $Pass, $CveSistema, $CveNivel, $Nombre);
      $sentencia->store_result();
      if($sentencia->fetch()===null){
         /*No hay datos*/
         $cve = 405;
         $text="No existe el usuario ".$txtNombre;
      }
      else{
         if(password_verify($txtPass, $Pass)){
            $_SESSION['MDE_ClaveGeneral'] = $Clave;
            $_SESSION['MDE_NombreCompleto'] = $Nombre;
            $_SESSION['MDE_CveSistema'] = $CveSistema;
            $_SESSION['MDE_NivelUsuario'] = $CveNivel;
            $text="Bienvenido <strong>$Nombre</strong> ";
            $cve=1;
         }
         else{
            $text="Contraseña incorrecta";
            $cve=506;
         }
      }
   }
   else{
      $cve=407;
      $text = "Falló la vinculación de parámetros: (" . $sentencia->errno . ") " . $sentencia->error;
   }
}
else{
   $cve=407;
   $text = "<br>Falló la preparación: (" . $bd->errno . ") " . $bd->error;
}
$Row ['text'] = $text;
$Row ['Cve']=$cve;
$Json[] = $Row;
echo json_encode($Json);
?>